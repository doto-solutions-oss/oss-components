import App from "./App.svelte";
export * from "./lib/VideoSlider.svelte";
export * from "./lib/InteractiveMap.svelte";

const app = new App({
  target: document.getElementById("app"),
});

export default app;
