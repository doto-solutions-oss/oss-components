import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import sveltePreprocess from "svelte-preprocess";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    rollupOptions: {
      input: [
        "./src/lib/VideoSlider.svelte",
        "./src/lib/InteractiveMap.svelte",
      ],
    },
  },
  plugins: [
    svelte({
      preprocess: [sveltePreprocess({ typescript: true })],

      // configFile: "svelte.config.js",
      // compilerOptions: {
      //   customElement: true,
      // },
    }),
  ],
});
